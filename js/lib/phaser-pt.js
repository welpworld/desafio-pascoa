/**
* @author       Diogo Cró <dcro@welpworld.com>
* @copyright    2017 welpworld
* @license      {@link https://github.com/photonstorm/phaser/blob/master/license.txt|MIT License}
*
* @overview
*
* v1.0 - 2017/01/27
*
* Phaser-pt é um conversão de algumas das funcionalidades disponíveis 
* no Phaser para português de forma a facilitar o desenvolvimento de 
* jogos por jovens portugueses
* 
* Phaser-pt uses phaser as base for HTML5 Game Framework
* Phaser is a HTML5 Game Framework created by Richard Davey and 
* you can find more on http://phaser.io/
* 
*/

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 welpworld
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
 /* global Phaser*/
var nulo = null;
var verdade = true;
var falso = false;
var NUMERO_MAXIMO = Number.MAX_VALUE;

var Motor = function () {
    this.verdade=true;  
    this.falso=false;


    this.tecla={
        'enter' : Phaser.KeyCode.ENTER,
        'cima' : Phaser.KeyCode.UP,
        'esquerda' : Phaser.KeyCode.LEFT,
        'direita' : Phaser.KeyCode.RIGHT,
        'baixo' : Phaser.KeyCode.DOWN,
        'espaco' : Phaser.KeyCode.SPACEBAR,
        'ctrl' : Phaser.KeyCode.CONTROL,
        'alt' : Phaser.KeyCode.ALT,
        'backspace' : Phaser.KeyCode.BACKSPACE,
        '\\' : Phaser.KeyCode.BACKWARD_SLASH,
        'capslock' : Phaser.KeyCode.CAPS_LOCK,
        '{' : Phaser.KeyCode.OPEN_BRACKET,
        '}' : Phaser.KeyCode.CLOSED_BRACKET,
        ':' : Phaser.KeyCode.COLON,
        ',' : Phaser.KeyCode.COMMA,
        '?' : Phaser.KeyCode.QUESTION_MARK,
        '.' : Phaser.KeyCode.PERIOD,
        '+' : Phaser.KeyCode.PLUS,
        '-' : Phaser.KeyCode.MINUS,
        'insert' : Phaser.KeyCode.INSERT,
        'home' : Phaser.KeyCode.HOME,
        'help' : Phaser.KeyCode.HELP,
        'pagedown' : Phaser.KeyCode.PAGE_DOWN,
        'pageup' : Phaser.KeyCode.PAGE_UP,
        'delete' : Phaser.KeyCode.DELETE,
        'end' : Phaser.KeyCode.END,
        '=' : Phaser.KeyCode.EQUALS,
        'esc' : Phaser.KeyCode.ESC,
        'f1' : Phaser.KeyCode.F1,
        'f2' : Phaser.KeyCode.F2,
        'f3' : Phaser.KeyCode.F3,
        'f4' : Phaser.KeyCode.F4,
        'f5' : Phaser.KeyCode.F5,
        'f6' : Phaser.KeyCode.F6,
        'f7' : Phaser.KeyCode.F7,
        'f8' : Phaser.KeyCode.F8,
        'f9' : Phaser.KeyCode.F9,
        'f10' : Phaser.KeyCode.F10,
        'f11' : Phaser.KeyCode.F11,
        'f12' : Phaser.KeyCode.F12,
        'f13' : Phaser.KeyCode.F13,
        'f14' : Phaser.KeyCode.F14,
        'f15' : Phaser.KeyCode.F15,
        '0' : Phaser.KeyCode.ZERO,
        '1' : Phaser.KeyCode.ONE,
        '2' : Phaser.KeyCode.TWO,
        '3' : Phaser.KeyCode.THREE,
        '4' : Phaser.KeyCode.FOUR,
        '5' : Phaser.KeyCode.FIVE,
        '6' : Phaser.KeyCode.SIX,
        '7' : Phaser.KeyCode.SEVEN,
        '8' : Phaser.KeyCode.EIGHT,
        '9' : Phaser.KeyCode.NINE,
        'numpad_0' : Phaser.KeyCode.NUMPAD_0,
        'numpad_1' : Phaser.KeyCode.NUMPAD_1,
        'numpad_2' : Phaser.KeyCode.NUMPAD_2,
        'numpad_3' : Phaser.KeyCode.NUMPAD_3,
        'numpad_4' : Phaser.KeyCode.NUMPAD_4,
        'numpad_5' : Phaser.KeyCode.NUMPAD_5,
        'numpad_6' : Phaser.KeyCode.NUMPAD_6,
        'numpad_7' : Phaser.KeyCode.NUMPAD_7,
        'numpad_8' : Phaser.KeyCode.NUMPAD_8,
        'numpad_9' : Phaser.KeyCode.NUMPAD_9,
        'numpad_+' : Phaser.KeyCode.NUMPAD_ADD,
        'numpad_-' : Phaser.KeyCode.NUMPAD_SUBTRACT,
        'numpad_*' : Phaser.KeyCode.NUMPAD_MULTIPLY,
        'numpad_/' : Phaser.KeyCode.NUMPAD_DIVIDE,
        'numpad_enter' : Phaser.KeyCode.NUMPAD_ENTER,
        'numlock' : Phaser.KeyCode.NUM_LOCK,
        'numpad_.' : Phaser.KeyCode.NUMPAD_DECIMAL,
        'tab' : Phaser.KeyCode.TAB,
        '_' : Phaser.KeyCode.UNDERSCORE,
        'a' : Phaser.KeyCode.A,
        'b' : Phaser.KeyCode.B,
        'c' : Phaser.KeyCode.C,
        'd' : Phaser.KeyCode.D,
        'e' : Phaser.KeyCode.E,
        'f' : Phaser.KeyCode.F,
        'g' : Phaser.KeyCode.G,
        'h' : Phaser.KeyCode.H,
        'i' : Phaser.KeyCode.I,
        'j' : Phaser.KeyCode.J,
        'k' : Phaser.KeyCode.K,
        'l' : Phaser.KeyCode.L,
        'm' : Phaser.KeyCode.M,
        'n' : Phaser.KeyCode.N,
        'o' : Phaser.KeyCode.O,
        'p' : Phaser.KeyCode.P,
        'q' : Phaser.KeyCode.Q,
        'r' : Phaser.KeyCode.R,
        's' : Phaser.KeyCode.S,
        't' : Phaser.KeyCode.T,
        'u' : Phaser.KeyCode.U,
        'v' : Phaser.KeyCode.V,
        'w' : Phaser.KeyCode.W,
        'x' : Phaser.KeyCode.X,
        'y' : Phaser.KeyCode.Y,
        'z' : Phaser.KeyCode.Z
    };

    this.propriedade={
        'verificarLimitesTela' : 'checkWorldBounds',
        'destruirForaTela' : 'outOfBoundsKill',
        'velocidadeX' : 'body.velocity.x',
        'velocidadeY' : 'body.velocity.y',
        'rodar' : 'angle'
    };
    
};

Motor.prototype = {
    tela: function(width, height, parent) {
        this.jogo = new Phaser.Game(width,height,Phaser.AUTO,parent);
        this.LARGURA = width;
        this.ALTURA = height;
        this.CENTRO_X = width/2;
        this.CENTRO_Y = height/2;

    },

    fundo: function(cor){
        this.jogo.stage.backgroundColor = cor;  
    },
    corFundo: function(cor){
        this.jogo.stage.backgroundColor = cor;  
    },
    fundo: function(cor){
        this.jogo.stage.backgroundColor = cor;  
    },

    adicionarEstado: function(nomeEstado, estado){
        this.jogo.state.add(nomeEstado, estado);
    },

    activarEstado: function(estado){
        this.jogo.state.start(estado);
    },

    ativarEstado: function(estado){
        this.jogo.state.start(estado);
    },
    mudarEstado: function(estado){
        this.jogo.state.start(estado);
    },


    carregarImagem: function(id, origem){
        return this.jogo.load.image(id, origem);
    },

    numeroToques: function(nToques){
        this.jogo.input.maxPointers = nToques;
    },

    barraCarregamento: function(imagem){
        return this.jogo.load.setPreloadSprite(imagem);
    },

    definirBarraCarregamento: function(imagem){
        return this.jogo.load.setPreloadSprite(imagem);
    },

    utilizarImagem: function(x, y, ancora, id){
        var imagem = this.jogo.add.sprite(x, y, id);
        imagem.anchor.setTo(ancora);
        return imagem;
    },

    paraComputador: function(){
        return this.jogo.device.desktop;
    },
    computador: function(){
        return this.jogo.device.desktop;
    },

    paraDispositivoMovel: function(){
        return !this.jogo.device.desktop;
    },
    dispositivoMovel: function(){
        return !this.jogo.device.desktop;
    },

    DimensoesMovel: function(minX, minY, maxX, maxY, horizontal){
        this.jogo.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.jogo.scale.minWidth = minX;
        this.jogo.scale.minHeight = minY;
        this.jogo.scale.maxWidth = maxX;
        this.jogo.scale.maxHeight = maxY;
        this.jogo.scale.forceLandscape = horizontal;
        this.jogo.scale.pageAlignHorizontally = true;
     
    },

    definirDimensoesMovel: function(minX, minY, maxX, maxY, horizontal){
        this.jogo.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.jogo.scale.minWidth = minX;
        this.jogo.scale.minHeight = minY;
        this.jogo.scale.maxWidth = maxX;
        this.jogo.scale.maxHeight = maxY;
        this.jogo.scale.forceLandscape = horizontal;
        this.jogo.scale.pageAlignHorizontally = true;
     
    },

    carregarAtlas: function(id, origemPNG, origemJSON){
        return this.jogo.load.atlas(id, origemPNG, origemJSON);
    },

    carregarSprite: function(id, origem, largura, altura, nImg){
        return this.jogo.load.spritesheet(id, origem, largura, altura, nImg);
    },

    carregarTextoBitmap: function(id, origemPNG, origemXML){
        return this.jogo.load.bitmapFont(id, origemPNG, origemXML);
    },

    carregarSom: function(id, origem){
        return this.jogo.load.audio(id, [origem[0], origem[1]]);
    },

    utilizarSom: function(id){
        return this.jogo.add.audio(id);
    },

    tocarSom: function(objecto, volume, repetir){
        objecto.play('',0,volume,repetir);
    },

    utilizarSprite: function(xInicial, yInicial, xFinal, yFinal, id){
        return this.jogo.add.tileSprite(xInicial, yInicial, xFinal, yFinal, id);
    },
    rotacaoImagem: function(imagem, rotacaoX, rotacaoY){
        imagem.autoScroll(rotacaoX, rotacaoY);
    },
    rotacaoSprite: function(imagem, rotacaoX, rotacaoY){
        imagem.autoScroll(rotacaoX, rotacaoY);
    },
    larguraTela: function(){
        return this.jogo.width;
    },

    alturaTela: function(){
        return this.jogo.height;
    },

    largura: function(objecto){
        if(objecto)
            return objecto.width;
        else
            return this.jogo.width;
    },

    altura: function(objecto){
        if(objecto)
            return objecto.height;
        else
            return this.jogo.height;
    },
    centroX: function(objecto){
        if(objecto===null)
            return this.jogo.world.centerX;
        return this.largura(objecto) / 2;
    },
    centroY: function(objecto){
        if(objecto===null)
             return this.jogo.world.centerY;
        return this.altura(objecto) / 2;
    },
    somDescodificado: function(id){
        return this.jogo.cache.isSoundDecoded(id);
    },

    carregamentoCompleto: function(metodo, id){
        this.jogo.load.onLoadComplete.add(metodo, id);
    },

    adicionarTexto: function(x, y, texto, estilo){
        return this.jogo.add.text(x,y,texto, estilo);
    },

    alterarTexto: function(objecto, texto){
        return objecto.text = texto;
    },

    adicionarTextoBitmap: function(x, y, id, texto, tamanho){
        return this.jogo.add.bitmapText(x, y, id, texto, tamanho);
    },

    teclaPressionada: function(idTecla){
        if(this.jogo.input.keyboard.isDown(this.tecla[idTecla]))
            return true;
        return false;
    },

    criarAnimacao: function(id, ordemAnimancao, objecto){
        objecto.animations.add(id, ordemAnimancao);
    },

    iniciarAnimacao: function(id,velocidade,repetir,objecto,destruir){
        objecto.animations.play(id, velocidade, repetir,destruir);
    },
    
    activarFisica: function(){
        this.jogo.physics.startSystem(Phaser.Physics.ARCADE);
    },

    ativarFisica: function(){
        this.jogo.physics.startSystem(Phaser.Physics.ARCADE);
    },

    utilizarFisica: function(objecto){
        this.jogo.physics.arcade.enableBody(objecto);
    },

    ligarFisica: function(objecto){
        this.jogo.physics.arcade.enableBody(objecto);
    },
    utilizarFisicaGrupo: function(grupo){
        grupo.enableBody = true;
        grupo.physicsBodyType = Phaser.Physics.ARCADE;
    },
    ligarFisicaGrupo: function(grupo){
        grupo.enableBody = true;
        grupo.physicsBodyType = Phaser.Physics.ARCADE;
    },
    ligarGravidade: function(objecto){
        objecto.body.allowGravity = true;
    },
    
    objectoPermiteFisica: function(objecto, valor){
        objecto.body.allowGravity = valor;
    },
    objetoPermiteFisica: function(objecto, valor){
        objecto.body.allowGravity = valor;
    },

    objectoImovel: function(objecto, valor){
        objecto.body.immovable = valor;
    },
    objetoImovel: function(objecto, valor){
        objecto.body.immovable = valor;
    },
    imovel: function(objecto){
        objecto.body.immovable = true;
    },
    
    colidirComLimites: function(objecto){
        objecto.body.collideWorldBounds = true;
    },

    objectoColideComLimites: function(objecto, valor){
        objecto.body.collideWorldBounds = valor;
    },
    objetoColideComLimites: function(objecto, valor){
        objecto.body.collideWorldBounds = valor;
    },

    definirGravidadeY: function(objecto, valor){
        objecto.body.gravity.y = valor;
    },

    gravidadeY: function(objecto, valor){
        objecto.body.gravity.y = valor;
    },

    definirGravidadeX: function(objecto, valor){
        objecto.body.gravity.x = valor;
    },
    gravidadeX: function(objecto, valor){
        objecto.body.gravity.x = valor;
    },

    definirVelocidadeY: function(objecto, valor){
        objecto.body.velocity.y = valor;
    },
    velocidadeY: function(objecto, valor){
        objecto.body.velocity.y = valor;
    },

    definirVelocidadeX: function(objecto, valor){
        objecto.body.velocity.x = valor;
    },
    velocidadeX: function(objecto, valor){
        objecto.body.velocity.x = valor;
    },

    tocarNoChao: function(objecto){
        return objecto.body.touching.down;
    },
    noChao: function(objecto){
        return objecto.body.touching.down;
    },

    espelharSprite: function(objecto, lado){
        if(lado === 'direita' && objecto.scale.x<0)
            objecto.scale.x *= -1;
        else if (lado === 'esquerda' && objecto.scale.x>0)
            objecto.scale.x *= -1;
    },
    espelhar: function(objecto, lado){
        if(lado === 'direita' && objecto.scale.x<0)
            objecto.scale.x *= -1;
        else if (lado === 'esquerda' && objecto.scale.x>0)
            objecto.scale.x *= -1;
    },

    definirEscalaObjecto: function(objecto, escala){
        objecto.scale.setTo(escala);
    },
    escala: function(objecto, escala){
        objecto.scale.setTo(escala);
    },
    definirEscalaObjeto: function(objecto, escala){
        objecto.scale.setTo(escala);
    },

    novoGrupo: function(){
        return this.jogo.add.group();    
    },
    
    colide: function(objecto, colisao){
        this.jogo.physics.arcade.collide(objecto, colisao);
    },

    sobreposicao: function(objecto, colisao, funcao, contexto){
        this.jogo.physics.arcade.overlap(objecto,colisao,funcao, null, contexto);
    },

    numeroAleatorio: function(inicio, fim){
        return this.jogo.rnd.integerInRange(inicio, fim);
    },

    adicionarElementoExistente: function(objecto){
        this.jogo.add.existing(objecto);
    },
    adicionarElemento: function(objecto){
        this.jogo.add.existing(objecto);
    },

    devolvePrimeiroElemento: function(objecto, valor){
        return objecto.getFirstExists(valor);
    },

    primeiroElemento: function(objecto, valor){
        return objecto.getFirstExists(valor);
    },
    

    tempoAgora: function(){
        return this.jogo.time.now;
    },

    tempo: function(){
        return this.jogo.time.now;
    },

    definirParaTodos: function(objecto, propriedade, valor){
        objecto.setAll(this.propriedade[propriedade], valor);
    }, 
    
    criarVarios: function(objecto, numeroObjectos, id){
        objecto.createMultiple(numeroObjectos, id);
    },

    destruirObjecto: function(objecto){
        objecto.kill();
    },
    destruirObjeto: function(objecto){
        objecto.kill();
    },

    destruirGrupo: function(objecto){
        objecto.destroy();
    },

    definirPosicao: function(objecto, x, y){
        objecto.reset(x,y);
    },

    posicao: function(objecto, x, y){
        objecto.reset(x,y);
    },

    adicionarRectangulo: function(xInicial, yInicial, xFinal, yFinal, cor, alpha){
        var bmd = this.jogo.add.bitmapData(xFinal-xInicial, yFinal-yInicial);
        bmd.ctx.fillStyle = cor;
        bmd.ctx.fillRect(xInicial, yInicial, xFinal, yFinal);
        var rectangulo = this.jogo.add.sprite(xInicial, yInicial, bmd);
        rectangulo.alpha = alpha;

        return rectangulo;
    },

    adicionarRetangulo: function(xInicial, yInicial, xFinal, yFinal, cor, alpha){
        var bmd = this.jogo.add.bitmapData(xFinal-xInicial, yFinal-yInicial);
        bmd.ctx.fillStyle = cor;
        bmd.ctx.fillRect(xInicial, yInicial, xFinal, yFinal);
        var rectangulo = this.jogo.add.sprite(xInicial, yInicial, bmd);
        rectangulo.alpha = alpha;

        return rectangulo;
    },
    retangulo: function(xInicial, yInicial, xFinal, yFinal, cor, alpha){
        var bmd = this.jogo.add.bitmapData(xFinal-xInicial, yFinal-yInicial);
        bmd.ctx.fillStyle = cor;
        bmd.ctx.fillRect(xInicial, yInicial, xFinal, yFinal);
        var rectangulo = this.jogo.add.sprite(xInicial, yInicial, bmd);
        rectangulo.alpha = alpha;

        return rectangulo;
    },
    
    numeroObjectosDestruidos: function(grupo){
        return grupo.countDead();
    },

    numeroObjetosDestruidos: function(grupo){
        return grupo.countDead();
    },
    objetosDestruidos: function(grupo){
        return grupo.countDead();
    },

    primeiroElementoDestruido: function(grupo){
        return grupo.getFirstDead();
    },
    elementoDestruido: function(grupo){
        return grupo.getFirstDead();
    },
    ancora: function(image, ancoraX,ancoraY){
        image.anchor.setTo(ancoraX,ancoraY);
    },
    definirAncora: function(image, ancoraX,ancoraY){
        image.anchor.setTo(ancoraX,ancoraY);
    },

    trazerParaCima: function(imagem){
        imagem.bringToTop();
    },
    numeroMaximo: function(){
        return Number.MAX_VALUE;
    },
    capturarTecla:function(tecla){
        this.jogo.input.keyboard.addKeyCapture(this.tecla[tecla]);
    },
    forcarOrientacao:function(horizontal,vertical){
        this.jogo.scale.forceOrientation(horizontal, vertical);
    },
    
    orientacaoCorrecta:function(funcao){
        this.jogo.scale.leaveIncorrectOrientation.add(funcao);
    },
    orientacaoIncorrecta:function(funcao){
        this.jogo.scale.enterIncorrectOrientation.add(funcao);
    },
    seguirApontador: function(objecto){
        objecto.rotation = this.jogo.physics.arcade.angleToPointer(objecto);
    },
    angulo: function(objecto){
        return objecto.angle;
    },
    definirAngulo:function(objecto,angulo) {
        objecto.angle = angulo;
        
    },
    velocidadeAngular:function(objecto,velocidade){
        this.jogo.physics.arcade.velocityFromAngle(objecto.angle, velocidade, objecto.body.velocity);
        
    }

};
