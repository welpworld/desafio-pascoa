Welpworld.Game = function() {
 

  this.vivo=verdade;
  this.reiniciar = falso;

  this.frequenciaObstaculo1 = 3000;
  this.proximoObstaculo1 = 0;
  this.proximoOvo1=0;
  this.frequenciaOvo1 = 1500;
  this.pontuacao1 = 0;
  this.pontuacao2 = 0;

  this.vencedor='';
};

Welpworld.Game.prototype = {
    create: function() {

        this.fundo = jogo.utilizarSprite(0,0,jogo.larguraTela(),jogo.alturaTela(),'fundo');
        this.horizonte1 = jogo.utilizarSprite(0,75,jogo.larguraTela(),200,'horizonte');
        this.relva1 = jogo.utilizarSprite(0,200,jogo.larguraTela(),51,'relva');
        this.chao1 = jogo.utilizarSprite(0,247,jogo.larguraTela(),53,'chao');
        
        jogo.rotacaoSprite(this.horizonte1,-50,0);    
        jogo.rotacaoSprite(this.chao1,-300,0);
        jogo.rotacaoSprite(this.relva1,-300,0);

       
        
        this.jogador1 = jogo.utilizarImagem(300, 100, 0.5 , 'jogador1');

        jogo.criarAnimacao("correr",[0,1,2],this.jogador1);
        jogo.criarAnimacao("saltar",[3,4,5],this.jogador1);

        jogo.activarFisica();
        jogo.ligarFisica(this.jogador1);
        jogo.ligarFisica(this.chao1);
        jogo.imovel(this.chao1);
        jogo.gravidadeY(this.jogador1,500);
        
        jogo.colidirComLimites(this.jogador1);

        this.obstaculos1 = jogo.novoGrupo();
            
        jogo.utilizarFisicaGrupo(this.obstaculos1);
        jogo.criarVarios(this.obstaculos1, 5, 'pedra');
        jogo.definirParaTodos(this.obstaculos1, 'verificarLimitesTela',verdade);
        jogo.definirParaTodos(this.obstaculos1, 'destruirForaTela', verdade);

        this.ovos1 = jogo.novoGrupo();
            
        jogo.utilizarFisicaGrupo(this.ovos1);
        jogo.criarVarios(this.ovos1, 5, 'ovo1');
        jogo.definirParaTodos(this.ovos1, 'verificarLimitesTela',verdade);
        jogo.definirParaTodos(this.ovos1, 'destruirForaTela', verdade);

        this.pontos1 = jogo.adicionarTextoBitmap(10,10,'minecraftia',"Jogador 1: " + this.pontuacao1,20);
    },
    update: function() {

        jogo.sobreposicao(this.jogador1,this.obstaculos1,this.jogador1ColidePedra1,this);
        jogo.sobreposicao(this.jogador1,this.ovos1,this.jogador1ColideOvo1,this);
        jogo.colide(this.jogador1,this.chao1);
       
        this.movimentoJogador1();

    
        if(jogo.tempo()>this.proximoObstaculo1 && jogo.objetosDestruidos(this.obstaculos1)>0 ){
            this.criarObstaculo1();        
            this.proximoObstaculo1 = jogo.tempo()+this.frequenciaObstaculo1;
        }
        if(jogo.tempo()>this.proximoOvo1 && jogo.objetosDestruidos(this.ovos1)>0 ){
            this.criarOvo1();        
            this.proximoOvo1 = jogo.tempo()+this.frequenciaOvo1;
        }



        if((jogo.teclaPressionada("r") || jogo.jogo.input.activePointer.isDown) && this.reiniciar===verdade)
            this.recomecar();

    },
  
    movimentoJogador1: function() {
        if(jogo.teclaPressionada('espaco') && jogo.tocarNoChao(this.jogador1)){
            jogo.velocidadeY(this.jogador1,-350);
            jogo.iniciarAnimacao("saltar",2.15,verdade,this.jogador1,falso);
        }else if(jogo.tocarNoChao(this.jogador1)){
            jogo.iniciarAnimacao("correr",5,verdade,this.jogador1,falso);
        }
            
    },
    criarObstaculo1:function(){
        var obs = jogo.elementoDestruido(this.obstaculos1);
        var escala = jogo.numeroAleatorio(1,2);
        jogo.escala(obs,escala);
        jogo.ancora(obs,1,.5);
        jogo.posicao(obs,jogo.LARGURA,220);
        jogo.velocidadeX(obs,-300);
    },
    criarOvo1:function(){
        var ovo = jogo.elementoDestruido(this.ovos1);
        jogo.ancora(ovo,.5,.5);
        jogo.posicao(ovo,jogo.LARGURA,170);
        jogo.velocidadeX(ovo,-300);
    },
    jogador1ColidePedra1:function(jogador,pedra){
        jogo.destruirObjeto(pedra);
        this.fimJogo();
        this.vivo=falso;
        
    },
    jogador1ColideOvo1:function(jogador,ovo){
        jogo.destruirObjeto(ovo);
        
        this.pontuacao1 = this.pontuacao1 + 1;
        jogo.alterarTexto(this.pontos1, "Jogador 1: " + this.pontuacao1);
    },
    escolherVencedor: function(){
        if(this.pontuacao1>this.pontuacao2)
            this.vencedor = 'jogador 1';
        else if(this.pontuacao1<this.pontuacao2)
            this.vencedor = 'jogador 2';
        else
            this.vencedor = 'Empatado';
    },
    destruirElementosJogador1:function(){
        jogo.destruirObjeto(this.jogador1);
        jogo.destruirGrupo(this.obstaculos1);
        jogo.destruirGrupo(this.ovos1);
           
        jogo.rotacaoSprite(this.horizonte1,0,0);    
        jogo.rotacaoSprite(this.chao1,0,0);
        jogo.rotacaoSprite(this.relva1,0,0);
    },
    fimJogo: function() {
        
        this.destruirElementosJogador1();
        this.escolherVencedor();
        
        this.vivo=jogo.falso;

        jogo.adicionarRectangulo(0,0,jogo.larguraTela(),jogo.alturaTela(),'#000',0.7)
        
        var texto="O vencedor foi: " + this.vencedor; 
        var pontuacao=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto,28);
        pontuacao.x = jogo.larguraTela() / 2 - jogo.largura(pontuacao) /2 ;
        pontuacao.y = jogo.alturaTela() / 2 - jogo.altura(pontuacao)  / 2 - 25;
        
        texto = '(Pressiona R para reiniciar)';
        var reiniciar=jogo.adicionarTextoBitmap(0,0,'minecraftia', texto,12);
        reiniciar.x = jogo.larguraTela() / 2 - jogo.largura(reiniciar) /2 ;
        reiniciar.y = jogo.alturaTela() / 2 - jogo.altura(reiniciar) / 2 + 50;

        this.reiniciar=jogo.verdade;
    },
    recomecar: function() {
     
        this.pontuacao1 = 0;
        this.proximoObstaculo1=0;
        this.proximoOvo1=0;
        this.reiniciar = falso;
        jogo.activarEstado('Game');
    }


};
