Welpworld.Preload = function() {
  this.pronto = jogo.falso;
  
};

Welpworld.Preload.prototype = {
  preload: function() {

    this.splash =jogo.utilizarImagem(jogo.centroX(), jogo.centroY(), 0.5, 'logo');
  
    this.barra = jogo.utilizarImagem(jogo.centroX(), jogo.centroY() + 150, 0.5, 'preloadbar');
    
    jogo.definirBarraCarregamento(this.barra);
 
    jogo.carregarImagem('horizonte', 'assets/images/horizonte.png');
    jogo.carregarImagem('chao', 'assets/images/chao.png');
    jogo.carregarImagem('fundo', 'assets/images/fundo.png');
    jogo.carregarImagem('pedra', 'assets/images/obstaculo.png');
    jogo.carregarImagem('relva', 'assets/images/relva.png');
    
    jogo.carregarImagem('ovo1', 'assets/images/ovo1.png');
    jogo.carregarImagem('ovo2', 'assets/images/ovo2.png');
    
    jogo.carregarSprite('jogador1', 'assets/images/jogador1.png', 94.8, 82, 6);
    jogo.carregarSprite('jogador2', 'assets/images/jogador2.png', 106, 82,6);

    jogo.carregarTextoBitmap('minecraftia', 'assets/fonts/minecraftia/minecraftia.png', 'assets/fonts/minecraftia/minecraftia.xml');

    //This needs something to load or won't be called'
    jogo.carregamentoCompleto(this.carregamentoCompleto,this);
  },
  
  update: function() {
   if( this.pronto === jogo.verdade ){
   jogo.activarEstado('MainMenu');
    }
  },
 
  carregamentoCompleto: function() {
    this.pronto = jogo.verdade;
  },

 
};